#!/usr/bin/python3
"""
Parses https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/stages.yml 
and prints out which users are in each group.
"""

from pprint import pprint
import requests
import yaml

def download_stages():
    """
    Downloads stages.yml and puts it into a dict object.
    It was faster to write this than to manually format the YAML into a python dict.
    """
    req = requests.get('https://gitlab.com/gitlab-com/www-gitlab-com/-/raw/master/data/stages.yml')
    if req.status_code == 200:
        return yaml.safe_load(req.text)
    else:
        print("Error downloading stages.yml")
        return None

def download_usernames():
    """
    Download and parse team.yml
    """
    req = requests.get('https://about.gitlab.com/company/team/team.yml')
    if req.status_code == 200:
        return yaml.safe_load(req.text)
    else:
        print("Error downloading team.yml")
        return None

def flatten_array(arr):
    """
    Given a list of strings, concatenate the contents of all sub-lists (if any).
    """
    new_arr = []
    for item in arr:
        if type(item) == list:
            for subitem in item:
                new_arr.append(subitem)
        else:
            new_arr.append(item)
    return new_arr

def parse_groups(stages):
    """
    Given the stages object from download_stages(), construct each group name and parse the group members and managers.  Returns an array of 'group' objects.  Each group object contains a key with the group name, and array of managers, and an array of members.'
    """
    groups = []
    manager_keys = ['frontend_engineering_manager', 'backend_engineering_manager']
    member_keys = ['pm', 'pmm', 'cm', 'support', 'sets',
            'pdm', 'ux', 'uxr', 'tech_writer', 'tw_backup',
            'appsec_engineer']
    # Hard-coding department name since it appears that stages.yml only contains one department
    dept_name = 'eng-dev'
    # strip off outer object to make easier to work with
    stages = stages['stages']
    for stage in stages:
        for group in stages[stage]['groups']:
            cur_group = {'group_members': [], 'group_managers': []}
            # Some names have '_' in them.
            cur_group['group_name'] = f'{dept_name}-{stage}-{group}'.replace('_','-')
            # sub-object from yaml file
            group_obj = stages[stage]['groups'][group]
            for member in member_keys:
                if member in group_obj:
                    cur_group['group_members'].append(group_obj[member])
            for manager in manager_keys:
                if manager in group_obj:
                    cur_group['group_managers'].append(group_obj[manager])
            groups.append(cur_group)
    return groups

def clean_up_data(groups):
    """
    Flatten arrays, remove any TBD's
    Takes the output of parse_groups and returns an array of the same format.
    """
    for group in groups:
        group['group_members'] = flatten_array(group['group_members'])
        group['group_managers'] = flatten_array(group['group_managers'])
        updated_group_members = []
        for member in group['group_members']:
            if 'TBD' not in member:
                updated_group_members.append(member)
        updated_group_managers = []
        for manager in group['group_managers']:
            if 'TBD' not in manager:
                updated_group_managers.append(manager)
        group['group_members'] = updated_group_members
        group['group_managers'] = updated_group_managers
    return groups

def find_gl_usernames(groups):
    """
    Looks up each user by name and puts their gitlab username in the array.
    """
    all_users = download_usernames()
    for group in groups:
        for user_type in ['member', 'manager']:
            group_members_with_username = []
            for person in group['group_' + user_type + 's']:
                found = False
                for user in all_users:
                    if user['name'] == person:
                        group_members_with_username.append({user_type + '_name': person, 'member_handle': user['gitlab']})
                        found = True
                if not found:
                    group_members_with_username.append({user_type + '_name': person, 'member_handle': 'unknown'})

            group['group_' + user_type + 's'] = group_members_with_username

    return groups

def main():
    """
    The main program
    """
    stages = download_stages()
    groups = parse_groups(stages)
    clean_groups = clean_up_data(groups)
    username_clean_groups = find_gl_usernames(clean_groups)
    pprint(username_clean_groups)

if __name__ == '__main__':
    main()
