Gitlab Group Parser
===================

This script parses a [yaml file](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/stages.yml) and prints out the team members and managers in each group.

Running locally
---------------

Make sure you have the python3 version of `requests` and `yaml` installed.

```
$ ./parse_dept_group.py
```

Running with Docker
-------------------

```
docker build .
docker run -i <image-id>
```

Gitlab CI is enabled for this project.  To run, make a commit or run the pipeline manually.

Known issues
------------

* Not all group names appear in `gl_dept_group.txt'.  Perhaps the [Gitlab Departpent Group Expected Values](https://about.gitlab.com/handbook/infrastructure-standards/labels-tags/#expected-values-5) needs to be updated?
* The script is slow.  The `find_gl_usernames` function has 4 nested for loops and is operating on a large dataset(team.yml).
