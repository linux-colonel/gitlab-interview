FROM python:3.7

RUN apt-get update && apt-get -y install python3-requests python3-yaml
COPY parse_dept_group.py /usr/local/bin
USER www-data
CMD parse_dept_group.py
